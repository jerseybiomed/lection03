package lecture.recursion

object Step05Bonus extends App {
  // multiple primitive cases, multiple transformation

  def fibRec(n: Int): Int = {
    def loop(x0: Int, x1: Int, c: Int): Int =
      if (c < 0) loop(x1 - x0, x0, c + 1)
      else if (c == 0) x0
      else if (c == 1) x1
      else loop(x1, x0 + x1, c - 1)

    loop(0, 1, n)
  }

  def fibWhile(n: Int, m: Int): Int = {
    ???
  }

  val fibs = for (n <- -5 to 5) yield fibRec(n)

  println(fibs)

}

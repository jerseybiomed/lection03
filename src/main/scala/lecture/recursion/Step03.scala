package lecture.recursion

object Step03 {
  // one primitive case, one transformation

  def powWhile(n: Int, m: Int): Int = {
    var acc = 1
    var c = 0
    while (c < m) {
      acc = acc * n
      c = c + 1
    }
    acc
  }

  def powRec(n: Int, m: Int): Int = {
    def loop(acc: Int, c: Int): Int =
      if (c == m) acc
      else loop(acc * n, c + 1)

    loop(1, 0)
  }

}

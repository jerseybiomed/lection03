package lecture.recursion

object Step02 {

  object recursiveProcess {

    def factorial(n: Int): Int =
      if (n == 1) 1
      else n * factorial(n - 1)

    factorial(6)
    6 * factorial(5)
    6 * (5 * factorial(4))
    6 * (5 * (4 * factorial(3)))
    6 * (5 * (4 * (3 * factorial(2))))
    6 * (5 * (4 * (3 * (2 * factorial(1)))))
    6 * (5 * (4 * (3 * (2 * 1))))
    6 * (5 * (4 * (3 * 2)))
    6 * (5 * (4 * 6))
    6 * (5 * 24)
    6 * 120
    720

  }

  object linearProcess {

    def factIter(product: Int, counter: Int, maxCount: Int): Int =
      if (counter > maxCount) product
      else factIter(counter * product, counter + 1, maxCount)

    def factorial(n: Int): Int = factIter(1, 1, n)

    factorial(6)
    factIter(1, 1, 6)
    factIter(1, 2, 6)
    factIter(2, 3, 6)
    factIter(6, 4, 6)
    factIter(24, 5, 6)
    factIter(120, 6, 6)
    factIter(720, 7, 6)
    720

  }

}

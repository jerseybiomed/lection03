package lecture.recursion

object Step04 extends App {
  // multiple transformations


  def powRec(n: Int, m: Int): Int = {
    def loop(acc: Int, b: Int, c: Int): Int =
      if (c == 0) acc
      else if (c % 2 == 0) loop(acc, b * b, c / 2)
      else loop(acc * b, b, c - 1)

    loop(1, n, m)
  }

  def powWhile(n: Int, m: Int): Int = {
    var acc = 1
    var b = n
    var c = m

    while (c > 0) {
      if (c % 2 == 0) {
        b = b * b
        c = c / 2
      } else {
        acc = acc * b
        c = c - 1
      }
    }
    acc
  }

  println(powRec(5, 6))
  println(powWhile(5, 6))

}

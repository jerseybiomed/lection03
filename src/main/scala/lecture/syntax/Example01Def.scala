package lecture.syntax

object Example01Def {
  def funky1(foo: String, bar: Int): String =
    foo + bar

  def funky2(foo: String, bar: Int): Unit =
    foo + bar

  def funky3(foo: String, bar: Int) =
    foo + bar

  //depecated
  def funky4(foo: String, bar: Int) {
    foo + bar
  }
}
